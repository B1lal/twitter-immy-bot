const Twit = require('twit');
const config = require('./config');

var T = new Twit(config);

T.get('account/verify_credentials', {
  include_entities: false,
  skip_status: true,
  include_email: false,
}, onAuthenticated)


const twitterHandle = '@ImranKhanPMBot';

const motivationalQuotes = [
  "Ghabrana Nahi Hai",
  "Teeka Lagao",
  "Main Inko Chorun Ga Nahi",
  "Rolounga Subb Ko ",
  "Germany Ka Border ",
  "I can safely say that you can win the next election in Pakistan.",
  "Gardi Main Chalounga",
  "Huurain dikh rahi hain",
  "Tabdeeli Aa Nahi Rahi, Tabdeeli Agayi Hai",
  "U-turns Lena Aik Azeem Leader Ki Nishani Hai",
  "@zartajgulwazir Kehti Hain Ke Meri Killer Smile Se Sare Doubts Clear Ho Jate Hain",
  "Aghar Mulk Me Mehangai Barh Rahi Hai Iska Matlab, Leader Corrupt Hain.",

]

function onAuthenticated(err) {
  if (err) {
    console.log(err)
  } else {
    console.log('Auth Successful');
    // sendTweet()

    let termsToTrack = ['@ImranKhanPMBot']
    let stream = T.stream('statuses/filter', { track: termsToTrack, tweet_mode: 'extended' })

    stream.on('tweet', function (tweet) {

      if (!tweet.retweeted_status && tweet.user.screen_name !== twitterHandle) {
        sendReply(tweet)
      }
    })
  }
}

// function isTweetExactMatch(text){
//   text = text.toLowerCase()
//   return termsToTrack.some(term => text.includes(term))
// }

function sendReply(tweet) {
  let screenName = tweet.user.screen_name;

  let response = `@${screenName} ${motivationalQuotes[Math.floor(Math.random() * motivationalQuotes.length)]} #PTI #PMIK #ImranKhan`;
  console.log(`Replied to ${screenName}`);
  T.post('statuses/update', {
    in_reply_to_status_id: tweet.id_str,
    status: response
  }, onTweeted)
}


let isAsleep = false;

function onTweeted(err) {
  if (err !== undefined) {
    console.log(err)
    if (err.code === 88) {
      console.log('exceeded rate limit')
      T.post('account/update_profile', {
        description: 'I\ve brought too much tabdeeli, time to rest. Try again later'
      }, onTweeted)
      isAsleep = true
    } else {
      if(isAsleep){
        isAsleep = false
        T.post('account/update_profile', {
          description: 'This is an Immy bot which automatically, shares PMIK infinite wisdom with us. '
        }, onTweeted)
      }
    }
  }
}